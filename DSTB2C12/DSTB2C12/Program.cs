﻿using System;
using System.Linq;

namespace DSTB2C12
{
    class Program
    {
        static void Main(string[] args)
        {
            // func delegate , action delegate , predicate delegate
            Calculator c1 = new Calculator();
            //Calculator.MyDelegate mD = c1.Sum;

            Action mDa = c1.Disply;
            Action<int> mDa1 = c1.Sum;
            Func<int, int ,int> mDf = c1.Sub;
            Predicate<int> mDp = c1.Sum;

            mDf+=c1.Sub;
            mDf+=c1.Multi;
            mDf+=c1.Sum;
            mDf+=c1.Sub;

            mDf.Invoke(10, 12);
            var result = mDf.GetInvocationList().Select(x => (int)x.DynamicInvoke(10, 12)).Sum();
            Console.WriteLine(result);


            //c1.Sum(10, 12);
            //c1.Sub(10, 12);
            //c1.Multi(10, 12);
        }
    }
}
