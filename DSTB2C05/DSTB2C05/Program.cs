﻿using System;

namespace DSTB2C05
{
    class Program
    {
        static void Main(string[] args)
        {
            BottleCap c1 = new BottleCap("Red", 3.5);
            //c1.SetColor("Red");
            //c1.SetRadius(5.1);
            //c1.Color="Red";
           // c1.Radius=2.0;

            WaterBottle w1 = new WaterBottle();
            w1.Height=10;
            w1.Width=5;
            w1.Name="Dew 1";
            w1.Cap=c1;
            ///w1.AddWaterAmount(1);
            w1.Display();

            WaterBottle w2 = new WaterBottle();
            w2.Height=20;
            w2.Width=10;
            w2.Name="Dew 2";
            //w2.Cap=c1;
            w2.Display();

            //w1=w2;
            //w1.Name="Dew 3";
            //w1.Display();
            //w2.Display();

            //w1=null;
            ////w1.Name="Dew 3";
            ////w1.Display();
            //w2.Display();
        }
    }
}
