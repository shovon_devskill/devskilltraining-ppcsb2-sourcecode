﻿using System;

namespace DSTB2C05
{
    class BottleCap
    {
        public BottleCap()
        {
            Color="Black";
            Radius=2.5;
            Console.WriteLine("Bottle Cap CTOR 1 is Called");
        }
        public BottleCap(string color)
        {
            Color=color;
            Radius=2.5;
            Console.WriteLine("Bottle Cap CTOR 2 is Called");
        }
        public BottleCap(string color, double r)
        {
            Color=color;
            Radius=r;
            Console.WriteLine("Bottle Cap CTOR 3 is Called");
        }
        //private string Color;
        //private double Radius;
        //public void SetColor(string Color)
        //{
        //    if (Color=="Red")
        //    {
        //        Color="Black";
        //    }
        //    else
        //    {
        //        this.Color=Color;
        //    }

        //}
        //public string GetColor()
        //{
        //    return Color;
        //}
        //public void SetRadius(double Radius)
        //{
        //    this.Radius=Radius;
        //}
        //public double GetRadius()
        //{
        //    return Radius;
        //}
        public string Color { get;private set; }
        public double Radius { get; set; }

        //private string _color;

        //public string Color
        //{
        //    get
        //    {
        //        return _color;
        //    }
        //    set
        //    {
        //        _color=value;
        //    }
        //}
    }
}