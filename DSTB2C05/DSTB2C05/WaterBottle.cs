﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB2C05
{
    class WaterBottle
    {
        public string Name;
        public double Height;
        public double Width;
        private int Amount;
        public BottleCap Cap;
        public void AddWaterAmount(int litre)
        {
            Amount+=litre;
        }
        public void RemoveWaterAmount(int litre)
        {
            Amount-=litre;
        }
        public void Display()
        {
            Console.WriteLine("Bottle Name ="+Name);
            Console.WriteLine("Height Name ="+Height);
            Console.WriteLine("Width Name ="+Width);
            Console.WriteLine("Water Amount ="+Amount);
            if (Cap != null)
            {
                Console.WriteLine("Cap Color ="+Cap.Color);
                Console.WriteLine("Cap Radius ="+Cap.Radius);
            }
            Console.WriteLine("=====================================");
        }
    }
}
