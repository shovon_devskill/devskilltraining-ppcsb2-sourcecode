﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DSTB02C11
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var s1 = new Student() { Name="Tamim", Roll=101 };
            var s2 = new Student() { Name="Shakib", Roll=102 };
            var s3 = new Student() { Name="Mushfique", Roll=103 };

            var c1 = new Course() { Name="C#", Code="DST1", Fee=6000 };
            var c2 = new Course() { Name="ASP.NET", Code="DST2", Fee=30000 };
            var c3 = new Course() { Name="PHP", Code="DST3", Fee=10000 };

            c2.Students=new List<Student>() { s1, s3 };
            c3.Students=new List<Student>() { s1, s2 };
            //s2.Courses=new List<Course>() { c2 };
            //s3.Courses=new List<Course>() { c2 };

            var studentList = new List<Student>() { s1, s2, s3 };
            var courseList = new List<Course>() { c1, c2, c3 };

            //int? age = 10;

            //int? newAge = age==null ? 18 : age;

            //if (age==null)
            //{
            //    newAge=18;
            //}
            //else
            //{
            //    newAge=age;
            //}
            //int newAge= age??18;

            //var result = courseList.Where(x=> x.Students != null ? x.Students.Any(y=> y.Roll==101) : x.Students != null);
            //var result = courseList.Where(x => x.Name=="ASP.NET").SelectMany(x => x.Students??Enumerable.Empty<Student>());
            var result = courseList.Where(x => x.Name=="ASP.NET" &&x.Name=="C#").SelectMany(x => x.Students??Enumerable.Empty<Student>());

            foreach (var item in result)
            {
                Console.WriteLine("Student Name "+item.Name);
                //if (item.Students != null)
                //{
                //    foreach (var s in item.Students)
                //    {
                //        Console.WriteLine("\tStudent Name "+s.Name);
                //    }
                //}
            }
        }
    }
}