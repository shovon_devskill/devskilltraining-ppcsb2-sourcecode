﻿namespace DSTInterfaceExerciseSolution.Plane
{
    internal class F22 : IPlane
    {
        public IWeapon PrimaryWeapon { get; set; }
        public IWeapon SecondaryWeapon { get; set; }
    }
}