﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTInterfaceExerciseSolution.Plane
{
    class Su35 : IPlane
    {
        public IWeapon PrimaryWeapon { get ; set ; }
        public IWeapon SecondaryWeapon { get ; set ; }
    }
}
