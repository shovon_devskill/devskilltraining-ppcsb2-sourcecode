﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTInterfaceExerciseSolution.Weapon
{
    class Gun : IWeapon
    {
        public void Attack()
        {
            Console.WriteLine("Gun Shooting");
        }
    }
}
