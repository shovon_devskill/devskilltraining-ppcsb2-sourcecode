﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTInterfaceExerciseSolution.Weapon
{
    class Bomb : IWeapon
    {
        public void Attack()
        {
            Console.WriteLine("Bomb Drop");
        }
    }
}
