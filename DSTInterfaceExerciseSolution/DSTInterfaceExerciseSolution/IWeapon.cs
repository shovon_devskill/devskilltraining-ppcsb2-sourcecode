﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTInterfaceExerciseSolution
{
    interface IWeapon
    {
        void Attack();
    }
}
