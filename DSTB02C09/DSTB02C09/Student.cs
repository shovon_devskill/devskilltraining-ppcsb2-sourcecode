﻿using System;

namespace DSTB02C09
{
    public static class Student
    {
        static Student()
        {
            Name="DU";
        }
        public static string Name { get; set; }
        public static int Roll { get; set; }
        public static string UniversityName { get; set; }
        public static void Display()
        {
            Console.WriteLine("University "+UniversityName);
            Show();
        }

        public static void Show()
        {
        }
    }
}