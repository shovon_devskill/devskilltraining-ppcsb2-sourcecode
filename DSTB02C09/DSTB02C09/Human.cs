﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB02C09
{
    struct Human
    {
        public Human(int age)
        {
            Age=age;
        }
        public int Age { get; set; }
    }
    //class ExHuman
    //{
    //    public virtual void Display()
    //    {

    //    }
    //}
    //class Human : ExHuman
    //{
    //    public readonly int Age = 5;
    //    public Human()
    //    {
    //        Age=10;
    //    }
    //    public sealed override void Display()
    //    {

    //    }
    //}
    //class Student : Human
    //{
    //    public  override void Display()
    //    {

    //    }
    //}
    
}
