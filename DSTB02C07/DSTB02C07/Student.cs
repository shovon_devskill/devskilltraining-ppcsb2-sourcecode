﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB02C07
{
    class Student : Human
    {
        public int Roll { get; set; }
        //public Address StudentAddress { get; set; }
        public Student()
        {
            Name="ABC";
            Console.WriteLine("Student CTOR is called");
        }
        public void Display()
        {
            Console.WriteLine("Student Display is called");
            //Console.WriteLine("Name ="+Name);
            //Console.WriteLine("Age ="+Age);
            /Console.WriteLine("Roll= "+Roll);
            //Console.WriteLine("Address Name= "+StudentAddress.Name);
        }

        //~Student()
        //{
        //    Console.WriteLine("Student Destroctor is called");
        //}
        //~Student() => Console.WriteLine("Student Destroctor is called");
    }
}
