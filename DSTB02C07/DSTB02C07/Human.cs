﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB02C07
{
    class Human
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Human()
        {
            Console.WriteLine("Human CTOR is called ");
        }
        public virtual void Display()
        {
            Console.WriteLine("Human Display is called");
            //Console.WriteLine("Name ="+Name);
            //Console.WriteLine("Age ="+Age);
        }
    }
}
