﻿using System;

namespace DSTB02C07
{
    class Program
    {
        static void Main(string[] args)
        {
            //Human h1 = new Human();
            //h1.Name="Jamal";
            //h1.Age=20;
            //h1.Display();

            //Address a1 = new Address();
            //a1.Name="Mirpur";
            //a1.HouseNo="";

            //Human s1 = new Human();
            //s1.Display();

            //Student s1 = new Student();
            //s1.Display();

            Human s1 = new Student();   //ok
            s1.Name="ABC";
            s1.Age=21;
            //s1.Roll=101;
            s1.Display();

            //Student s2 = new Student();
            //s2.Name="Kamal";
            //s2.Age=21;
            //s2.Roll=101;
            //s2.StudentAddress=a1;
            //s2.Display();

        }
        //static void Call()
        //{
        //    Student s = new Student();
        //    s.Display();

        //    Student s1 = new Student();
        //    s1.Display();

        //    Student s2 = new Student();
        //    s2.Display();
        //}
    }
}
