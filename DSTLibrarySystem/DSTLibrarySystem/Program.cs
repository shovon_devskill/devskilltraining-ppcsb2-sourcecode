﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DSTLibrarySystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to ABC Library System");
            var libraryModel = new LibraryModel();

            while (true)
            {
                OptionMessage();
                Console.Write("Please Select an Option :");

                int option = Convert.ToInt32(Console.ReadLine());
                if (option==1)
                {
                    libraryModel.AddBook();
                }
                else if (option==2)
                {
                    libraryModel.BorrowBook();
                }
                else if (option==3)
                {
                    libraryModel.ReturnBook();
                }
                else if (option==4)
                {
                    libraryModel.DisplayBook();
                }
                else 
                {
                    break;
                }
                Console.WriteLine("======================================");
                
            }
            Console.WriteLine("Thank You for using ABC Library System");
        }
        public static void OptionMessage()
        {
            Console.ForegroundColor=ConsoleColor.Blue;
            Console.WriteLine("1: Add Book");
            Console.WriteLine("2: Borrow Book");
            Console.WriteLine("3: Return Book");
            Console.WriteLine("4: Display List");
            Console.WriteLine("5: Exit");
            Console.ResetColor();
        }
    }
}
