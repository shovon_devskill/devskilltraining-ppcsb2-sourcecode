﻿namespace DSTLibrarySystem
{
    public class Book
    {
        public Book(string code, string name, string author, int stock)
        {
            Code=code;
            Name=name;
            Author=author;
            Stock=stock;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public string Author { get; private set; }
        public int Stock { get; private set; }
        public void BorrowBook(int quantity)
        {
            Stock-=quantity;
        }

        public void AddBook(int quantity)
        {
            Stock+=quantity;
        }
    }
}