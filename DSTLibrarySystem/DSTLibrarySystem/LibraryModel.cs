﻿using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DSTLibrarySystem
{
    public class LibraryModel : ILibraryModel
    {
        private IList<Book> _bookList;
        public LibraryModel()
        {
            _bookList=new List<Book>();
        }

        public void AddBook()
        {
            Console.Write("Enter Book Code: ");
            string code = Console.ReadLine();
            Console.Write("Enter Book Name: ");
            string name = Console.ReadLine();
            Console.Write("Enter Author Name: ");
            string author = Console.ReadLine();
            Console.Write("Enter Initial Stock: ");
            int stock = Convert.ToInt32(Console.ReadLine());

            Book newBook = new Book(code, name, author, stock);
            _bookList.Add(newBook);
            Console.ForegroundColor=ConsoleColor.Green;
            Console.WriteLine("Successfully Added");
            Console.ResetColor();
        }

        public void BorrowBook()
        {
            Console.Write("Enter Book Code: ");
            string borrowCode = Console.ReadLine();

            Book searchBook = _bookList.Where(x => x.Code==borrowCode).FirstOrDefault();
            if (searchBook!=null)
            {
                Console.Write("Enter Book Stock: ");
                int borrowStock = Convert.ToInt32(Console.ReadLine());

                if (searchBook.Stock>=borrowStock)
                {
                    searchBook.BorrowBook(borrowStock);
                    //searchBook.Stock-=borrowStock;
                    Console.ForegroundColor=ConsoleColor.Green;
                    Console.WriteLine("Successfully Borrowed");
                }
                else
                {
                    Console.ForegroundColor=ConsoleColor.Red;
                    Console.WriteLine("Stock not avaliable");
                }
            }
            else
            {
                Console.ForegroundColor=ConsoleColor.Red;
                Console.WriteLine("Book not Found");
            }
            Console.ResetColor();
        }

        public void ReturnBook()
        {
            Console.Write("Enter Book Code: ");
            string returnCode = Console.ReadLine();

            Book searchBook = _bookList.Where(x => x.Code==returnCode).FirstOrDefault();
            if (searchBook!=null)
            {
                Console.Write("Enter Return Stock: ");
                int returnStock = Convert.ToInt32(Console.ReadLine());
                searchBook.AddBook(returnStock);
                //searchBook.Stock+=returnStock;
                Console.ForegroundColor=ConsoleColor.Green;
                Console.WriteLine("Successfully Returned");
            }
            else
            {
                Console.ForegroundColor=ConsoleColor.Red;
                Console.WriteLine("Book not Found");
            }
            Console.ResetColor();
        }

        public void DisplayBook()
        {
            var table = new ConsoleTable("Code", "Name", "Author", "Stock");

            foreach (var item in _bookList)
            {
                table.AddRow(item.Code, item.Name, item.Author, item.Stock);
            }
            table.Write();
        }
    }
}