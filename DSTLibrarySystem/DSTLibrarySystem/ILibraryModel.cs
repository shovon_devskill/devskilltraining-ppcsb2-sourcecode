﻿namespace DSTLibrarySystem
{
    public interface ILibraryModel
    {
        void AddBook();

        void BorrowBook();

        void ReturnBook();

        void DisplayBook();
    }
}