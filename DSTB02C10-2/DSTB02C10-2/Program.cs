﻿using System;
using System.Collections.Generic;

namespace DSTB02C10_2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //var h1 = new Human()
            //{
            //    Name="Jamal",
            //    Age=20,
            //};
            //var h2 = new Human()
            //{
            //    Name="Kamal",
            //    Age=25,
            //};

            ////var list = new List<Human>();
            ////list.Add(h1);
            ////list.Add(h2);
            ///LINQ
            var list = new List<Human>
            {
                new Human(){ Name="Jamal", Age=20,},
                new Human(){ Name="Kamal", Age=25,},
            };

            foreach (var item in list)
            {
                Console.WriteLine("Name "+item.Name);
                Console.WriteLine("Age "+item.Age);
                //item.Display();
            }

            /*var sl = new SortedList<>();
            sl.Add(1, "One");
            sl.Add(3, "Devskill");
            sl.Add(4, "Devskill 2");

            foreach (var item in sl.Keys)
            {
                Console.WriteLine("Key ="+item);
                Console.WriteLine("Value ="+sl[item]);
            }*/
        }
    }
}