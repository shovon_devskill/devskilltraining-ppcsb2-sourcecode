﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB02C10_2
{
    class Human
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public void Display()
        {
            Console.WriteLine("Name is "+Name);
            Console.WriteLine("Age is "+Age);
        }
    }
}
