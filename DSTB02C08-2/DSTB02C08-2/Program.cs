﻿using System;

namespace DSTB02C08_2
{
    class Program
    {
        static void Main(string[] args)
        {
            CourseModel model = new CourseModel();
            model.Show(new OneOneCourse());
        }
    }
}
