﻿using System;

namespace DSTEFCoreWithGenericsSample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //InsetDataWithEfCore();
            InsetDataWithGenericRepository();
        }

        private static void InsetDataWithEfCore()
        {
            var db = new MyContext();
            var student = new Student
            {
                Name="Shovon 3",
                Age=25,
            };
            db.Students.Add(student);
            db.SaveChanges();
            Console.WriteLine("Successfully Data Inserted");
        }

        private static void InsetDataWithGenericRepository()
        {
            var student = new Student
            {
                Name="Shovon 4",
                Age=25,
            };
            var repository = new GenericRepository<Student>();
            repository.Insert(student);
            repository.Save();
            Console.WriteLine("Successfully Data Inserted");
        }
    }
}