﻿using Microsoft.EntityFrameworkCore;

namespace DSTEFCoreWithGenericsSample
{
    public class MyContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=SHOVON-PC;Database=DSTStudentDB;Integrated Security=True");
        }
    }
}