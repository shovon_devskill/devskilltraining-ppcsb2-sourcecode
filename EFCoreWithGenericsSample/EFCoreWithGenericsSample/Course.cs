﻿namespace DSTEFCoreWithGenericsSample
{
    public class Course : Entity
    {
        public string Name { get; set; }
        public int Price { get; set; }
    }
}