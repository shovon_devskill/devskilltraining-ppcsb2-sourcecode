﻿using System;

namespace DSTEFCoreWithGenericsSample
{
    public class Entity
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public Entity()
        {
            Id=new Random().Next(0, 100);
            CreatedOn=DateTime.Now;
        }
    }
}