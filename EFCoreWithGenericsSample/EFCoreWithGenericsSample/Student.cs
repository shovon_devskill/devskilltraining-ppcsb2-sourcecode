﻿namespace DSTEFCoreWithGenericsSample
{
    public class Student : Entity
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}