﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DSTEFCoreWithGenericsSample
{
    public class GenericRepository<T> where T : Entity
    {
        private MyContext _context = null;
        private DbSet<T> table = null;
        public GenericRepository()
        {
            this._context=new MyContext();
            table=_context.Set<T>();
        }

        public void Insert(T obj)
        {
            // with EF
            table.Add(obj);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}