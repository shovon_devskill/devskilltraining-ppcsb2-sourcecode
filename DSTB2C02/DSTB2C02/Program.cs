﻿using System;

namespace DSTB2C02
{
    class Student
    {
        public Guid Id { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Console.Write("Hello"); 
            //Console.WriteLine("Hello World!");

            //int i = 0;
            //bool b = false;
            //float f = 10.5f;
            //double d = 10.5;
            //decimal dc = 11.5m;

            //object o = 10.5;
            //dynamic dy = 10.5;

            //var v= 10;

            //Console.WriteLine(v.GetType());

            //int? age = null;
            Guid g = Guid.NewGuid();
            Console.WriteLine(g);

            DateTime d = new DateTime(2019,10,5);
            Console.WriteLine(d);

            //string name;
            //name= Console.ReadLine();

            //double age;
            //age=Convert.ToDouble(Console.ReadLine());

            //Console.WriteLine("Name is "+name+" Age is "+age.GetType());
            //Console.WriteLine("Name is {0} Age is {1}", name, age);
            //Console.WriteLine($"Name is {name} Age is {age}");

            //int a;
            //a=Console.Read();
            //Console.WriteLine(a);

            //ConsoleKeyInfo b;
            //b=Console.ReadKey();
            //Console.WriteLine(b.Key);
        }
    }
}
