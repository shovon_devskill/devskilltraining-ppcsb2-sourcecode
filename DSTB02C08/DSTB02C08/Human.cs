﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB02C08
{
    abstract class Human
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public abstract  void Display();
    }
}
