﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB2C13
{
    class MyDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder dbContextoptionbuilder)
        {
            dbContextoptionbuilder.UseSqlServer("Server=SHOVON-PC;Database=DSTBookDB;Integrated Security=True");
        }
    }
}
