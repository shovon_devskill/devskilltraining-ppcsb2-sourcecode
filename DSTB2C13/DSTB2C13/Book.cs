﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSTB2C13
{
    class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stock { get; set; }
        public Book(string name, int stock)
        {
            Name=name;
            Stock=stock;
        }
        public void Show()
        {
            Console.Write("Name "+Name);
            Console.WriteLine("\t Stock "+Stock);
        }
    }
}
