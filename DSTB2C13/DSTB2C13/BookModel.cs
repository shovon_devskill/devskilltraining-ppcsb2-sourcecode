﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DSTB2C13
{
    internal class BookModel
    {
        public void Add()
        {
            string bookName = "C# 6";
            int bookSotck = 10;
            string connectionString = "Server=SHOVON-PC;Database=DSTBookDB;Integrated Security=True";
            string sql = "Insert_by_SP";

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(sql, connection))
                {
                    var nameP = new SqlParameter("@Name", System.Data.SqlDbType.NVarChar, 10);
                    nameP.Value=bookName;
                    var stockP = new SqlParameter("@Stock", System.Data.SqlDbType.Int);
                    stockP.Value=bookSotck;

                    command.Parameters.Add(nameP);
                    command.Parameters.Add(stockP);
                    command.CommandType=System.Data.CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<Book> Display()
        {
            string connectionString = "Server=SHOVON-PC;Database=DSTBookDB;Integrated Security=True";
            string sql = "SELECT * FROM Books";
            List<Book> bookList = new List<Book>();

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string name = reader["Name"].ToString();
                            int stock = Convert.ToInt32(reader["Stock"].ToString());
                            Book b = new Book(name, stock);
                            bookList.Add(b);
                        }
                    }
                }
            }
            return bookList;
        }
    }
}