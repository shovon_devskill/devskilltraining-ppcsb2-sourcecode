﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSTB2C13
{
    class GenericRepository<T> where T : class
    {
        private MyDbContext _dbContext;
        private DbSet<T>  _dbset;
        public GenericRepository()
        {
            _dbContext=new MyDbContext();
            _dbset=_dbContext.Set<T>();
        }
        public void Insert(T obj)
        {
            _dbset.Add(obj);
            _dbContext.SaveChanges();
        }
        public void Update(T obj)
        {
            _dbset.Attach(obj);
            _dbContext.Entry(obj).State=EntityState.Modified;
            _dbContext.SaveChanges();
        }
        public void Delete(T obj)
        {
            _dbset.Remove(obj);
            _dbContext.SaveChanges();
        }
        public IEnumerable<T> Get()
        {
            return _dbset.ToList();
        }
        public T GetByID(object id)
        {
            return _dbset.Find(id);
        }
    }
}
